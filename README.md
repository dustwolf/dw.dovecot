# Ansible Dovecot

Ansible role for installing the Dovecot mailbox server.

Currently made to work with CentOS 8 stream Linux.

## Note

Because of the somewhat circular nature of the dependency on certbot, the service might not work when first installed. Simply restart it later once everything is in place.

## Project status
This is a work in progress. I am using this to manage my own private servers and I will contribute as time allows.

You may use this code if you find it useful.

## Dependencies

Requires dw.certbot: https://gitlab.com/dustwolf/dw.certbot
Requires dw.postfix: https://gitlab.com/dustwolf/dw.postfix
